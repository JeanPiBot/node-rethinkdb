const express = require('express');

const app = express();

const cors = require('cors');

const { connection, close } = require('./rethinkdb/rethinkdb');

const people = require('./routes/people');

const port = 5000;

app.use(cors());
app.use(express.json());

app.use(connection);
app.use('/api/people', people);
app.use(close);

app.listen(port, () => {
    console.log(`server is running http://localhost:${port}`);
});
