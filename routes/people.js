const express = require('express');

const router = express.Router();

// database
const { rethinkdb, desc } = require('../rethinkdb/rethinkdb');

// add a person
router.post('/', async (req, res) => {
    try {
        const datos = req.body;
        await rethinkdb.table('people').insert(datos).run(req.rdb);
        res.status(201).send('Person was added!');
    } catch (error) {
        res.status(400).send(error);
    }
});

// get people
router.get('/', async (req, res) => {
    try {
        const people = await rethinkdb.table('people').orderBy(desc).run(req.rdb);
        res.status(200).json({ success: true, data: people });
    } catch (error) {
        res.status(400).send(error);
    }
});
// get a single person
router.get('/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const getPerson = await rethinkdb.table('people').get(id).run(req.rdb);
        res.status(200).json({ success: true, data: getPerson });
    } catch (error) {
        res.status(404).send(error);
    }
});
// Update a data
router.put('/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const datos = req.body;
        await rethinkdb.table('people').get(id).update(datos).run(req.rdb);
        res.status(200).send('Person was completed update!');
    } catch (error) {
        res.status(404).send(error);
    }
});
// delete a person
router.delete('/:id', async (req, res) => {
    try {
        const { id } = req.params;
        await rethinkdb.table('people').get(id).delete().run(req.rdb);
        res.status(200).send('Person was deleted!');
    } catch (error) {
        res.status(404).send(error);
    }
});

module.exports = router;
